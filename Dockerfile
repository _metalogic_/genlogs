FROM golang:1.14 as builder

ENV GOPRIVATE github.com/EducationPlannerBC/*,bitbucket.org/_metalogic_/*

COPY ./ /build

WORKDIR /build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o genlog .

FROM metalogic/alpine:latest

WORKDIR /run

COPY --from=builder /build/genlog .

CMD ["./genlog"]
