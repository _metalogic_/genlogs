package main

import (
	"time"

	"bitbucket.org/_metalogic_/log"
)

func main() {
	log.SetLevel(log.TraceLevel)
	for {
		log.Debugf("generated log at %s", time.Now())
		time.Sleep(15 * time.Second)
	}
}
